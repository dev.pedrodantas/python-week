"""
Hello World Multi Linguas.

Dependendo da lingua configurada no ambiente o programa exibe
a mensagem correspondente.

Como usar:

Tenha a variavel LANG devidamente configurada.

Exemplo:

    export LANG=pt_BR

Execucao:

    python3 hello.py
    ou
    ./hello.py
"""
__version__ = "0.0.1"
__author__ = "Pedro Dantas"

import os

current_language = os.getenv("LANGUAGE", "en_US")[:5]
msg = "Hello, world!"

if current_language == "pt_BR":
    msg = "Ola, mundo!"
elif current_language == "it_IT":
    msg = "Ciao, mondo!"

print(msg)
